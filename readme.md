Conecta 4 con patrones
====================

Esta práctica ha sido realizada por los autores Luis Boto, Lourdes Morente y José Manuel Ramos.

Se incluyen dos versiones del código: la primera utilizando vista pasiva y la segunda 
implementando el patrón proxy. En ambas versiones se ha partido del código que
implementaba los patrones Composite, Command y Memento, que a su vez se basaban en el
código plantilla del Conecta 4 subido al aula virtual. 

Cabe destacar también el uso que se
ha hecho del código del juego Tic-Tac-Toe del repositorio:
https://github.com/USantaTecla-project-ticTacToe/java.swing.socket.sql.

En general nos sentimos satisfechos con el trabajo realizado y con la aplicación práctica
de los conceptos teóricos aprendidos en clase.

